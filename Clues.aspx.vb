﻿Public Class Clues
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then GetClues()
    End Sub
    Private Sub GetClues()
        Dim ds As DataSet = GeneralDataSet("select * from clues order by id")
        With ds.Tables(0)
            With .Rows(0)
                tbClue1.Text = .Item("Clue").ToString
                tbAnswer1.Text = .Item("Answer").ToString
                S11.Text = .Item("Order1").ToString
                S12.Text = .Item("Order2").ToString
                S13.Text = .Item("Order3").ToString
                S14.Text = .Item("Order4").ToString
                S15.Text = .Item("Order5").ToString
                S16.Text = .Item("Order6").ToString
                S17.Text = .Item("Order7").ToString
                S18.Text = .Item("Order8").ToString
                S19.Text = .Item("Order9").ToString
                S110.Text = .Item("Order10").ToString
            End With
            With .Rows(1)
                tbClue2.Text = .Item("Clue").ToString
                tbAnswer2.Text = .Item("Answer").ToString
                S21.Text = .Item("Order1").ToString
                S22.Text = .Item("Order2").ToString
                S23.Text = .Item("Order3").ToString
                S24.Text = .Item("Order4").ToString
                S25.Text = .Item("Order5").ToString
                S26.Text = .Item("Order6").ToString
                S27.Text = .Item("Order7").ToString
                S28.Text = .Item("Order8").ToString
                S29.Text = .Item("Order9").ToString
                S210.Text = .Item("Order10").ToString
            End With
            With .Rows(2)
                tbClue3.Text = .Item("Clue").ToString
                tbAnswer3.Text = .Item("Answer").ToString
                S31.Text = .Item("Order1").ToString
                S32.Text = .Item("Order2").ToString
                S33.Text = .Item("Order3").ToString
                S34.Text = .Item("Order4").ToString
                S35.Text = .Item("Order5").ToString
                S36.Text = .Item("Order6").ToString
                S37.Text = .Item("Order7").ToString
                S38.Text = .Item("Order8").ToString
                S39.Text = .Item("Order9").ToString
                S310.Text = .Item("Order10").ToString
            End With
            With .Rows(3)
                tbClue4.Text = .Item("Clue").ToString
                tbAnswer4.Text = .Item("Answer").ToString
                S41.Text = .Item("Order1").ToString
                S42.Text = .Item("Order2").ToString
                S43.Text = .Item("Order3").ToString
                S44.Text = .Item("Order4").ToString
                S45.Text = .Item("Order5").ToString
                S46.Text = .Item("Order6").ToString
                S47.Text = .Item("Order7").ToString
                S48.Text = .Item("Order8").ToString
                S49.Text = .Item("Order9").ToString
                S410.Text = .Item("Order10").ToString
            End With
            With .Rows(4)
                tbClue5.Text = .Item("Clue").ToString
                tbAnswer5.Text = .Item("Answer").ToString
                S51.Text = .Item("Order1").ToString
                S52.Text = .Item("Order2").ToString
                S53.Text = .Item("Order3").ToString
                S54.Text = .Item("Order4").ToString
                S55.Text = .Item("Order5").ToString
                S56.Text = .Item("Order6").ToString
                S57.Text = .Item("Order7").ToString
                S58.Text = .Item("Order8").ToString
                s59.Text = .Item("Order9").ToString
                S510.Text = .Item("Order10").ToString
            End With
            With .Rows(5)
                tbClue6.Text = .Item("Clue").ToString
                tbAnswer6.Text = .Item("Answer").ToString
                S61.Text = .Item("Order1").ToString
                S62.Text = .Item("Order2").ToString
                S63.Text = .Item("Order3").ToString
                S64.Text = .Item("Order4").ToString
                S65.Text = .Item("Order5").ToString
                S66.Text = .Item("Order6").ToString
                S67.Text = .Item("Order7").ToString
                S68.Text = .Item("Order8").ToString
                S69.Text = .Item("Order9").ToString
                S610.Text = .Item("Order10").ToString
            End With
            With .Rows(6)
                tbClue7.Text = .Item("Clue").ToString
                tbAnswer7.Text = .Item("Answer").ToString
                S71.Text = .Item("Order1").ToString
                S72.Text = .Item("Order2").ToString
                S73.Text = .Item("Order3").ToString
                S74.Text = .Item("Order4").ToString
                S75.Text = .Item("Order5").ToString
                S76.Text = .Item("Order6").ToString
                S77.Text = .Item("Order7").ToString
                S78.Text = .Item("Order8").ToString
                S79.Text = .Item("Order9").ToString
                S710.Text = .Item("Order10").ToString
            End With
            With .Rows(7)
                tbClue8.Text = .Item("Clue").ToString
                tbAnswer8.Text = .Item("Answer").ToString
                S81.Text = .Item("Order1").ToString
                S82.Text = .Item("Order2").ToString
                S83.Text = .Item("Order3").ToString
                S84.Text = .Item("Order4").ToString
                S85.Text = .Item("Order5").ToString
                S86.Text = .Item("Order6").ToString
                S87.Text = .Item("Order7").ToString
                S88.Text = .Item("Order8").ToString
                S89.Text = .Item("Order9").ToString
                S810.Text = .Item("Order10").ToString
            End With
            With .Rows(8)
                tbClue9.Text = .Item("Clue").ToString
                tbAnswer9.Text = .Item("Answer").ToString
                S91.Text = .Item("Order1").ToString
                S92.Text = .Item("Order2").ToString
                S93.Text = .Item("Order3").ToString
                S94.Text = .Item("Order4").ToString
                S95.Text = .Item("Order5").ToString
                S96.Text = .Item("Order6").ToString
                S97.Text = .Item("Order7").ToString
                S98.Text = .Item("Order8").ToString
                S99.Text = .Item("Order9").ToString
                S910.Text = .Item("Order10").ToString
            End With
            With .Rows(9)
                tbClue10.Text = .Item("Clue").ToString
                tbAnswer10.Text = .Item("Answer").ToString
                S101.Text = .Item("Order1").ToString
                S102.Text = .Item("Order2").ToString
                S103.Text = .Item("Order3").ToString
                S104.Text = .Item("Order4").ToString
                S105.Text = .Item("Order5").ToString
                S106.Text = .Item("Order6").ToString
                S107.Text = .Item("Order7").ToString
                S108.Text = .Item("Order8").ToString
                S109.Text = .Item("Order9").ToString
                S1010.Text = .Item("Order10").ToString
            End With
        End With
    End Sub
    Private Sub SaveClues()
        Dim sQuery As String = "Update clues set clue='" + tbClue1.Text.ToString.Replace("'", "''") + "',Answer='" + tbAnswer1.Text.ToString.Replace("'", "''") + "',order1=" + Val(S11.Text.ToString).ToString + ",order2=" + Val(S12.Text.ToString).ToString + ",order3=" + Val(S13.Text.ToString).ToString + ",order4=" + Val(S14.Text.ToString).ToString + ",order5=" + Val(S15.Text.ToString).ToString + ",order6=" + Val(S16.Text.ToString).ToString + ",order7=" + Val(S17.Text.ToString).ToString + ",order8=" + Val(S18.Text.ToString).ToString + ",order9=" + Val(S19.Text.ToString).ToString + ",order10=" + Val(S110.Text.ToString).ToString + " where id=1"
        sQuery = sQuery + " Update clues set clue='" + tbClue2.Text.ToString.Replace("'", "''") + "',Answer='" + tbAnswer2.Text.ToString.Replace("'", "''") + "',order1=" + Val(S21.Text.ToString).ToString + ",order2=" + Val(S22.Text.ToString).ToString + ",order3=" + Val(S23.Text.ToString).ToString + ",order4=" + Val(S24.Text.ToString).ToString + ",order5=" + Val(S25.Text.ToString).ToString + ",order6=" + Val(S26.Text.ToString).ToString + ",order7=" + Val(S27.Text.ToString).ToString + ",order8=" + Val(S28.Text.ToString).ToString + ",order9=" + Val(S29.Text.ToString).ToString + ",order10=" + Val(S210.Text.ToString).ToString + " where id=2"
        sQuery = sQuery + " Update clues set clue='" + tbClue3.Text.ToString.Replace("'", "''") + "',Answer='" + tbAnswer3.Text.ToString.Replace("'", "''") + "',order1=" + Val(S31.Text.ToString).ToString + ",order2=" + Val(S32.Text.ToString).ToString + ",order3=" + Val(S33.Text.ToString).ToString + ",order4=" + Val(S34.Text.ToString).ToString + ",order5=" + Val(S35.Text.ToString).ToString + ",order6=" + Val(S36.Text.ToString).ToString + ",order7=" + Val(S37.Text.ToString).ToString + ",order8=" + Val(S38.Text.ToString).ToString + ",order9=" + Val(S39.Text.ToString).ToString + ",order10=" + Val(S310.Text.ToString).ToString + " where id=3"
        sQuery = sQuery + " Update clues set clue='" + tbClue4.Text.ToString.Replace("'", "''") + "',Answer='" + tbAnswer4.Text.ToString.Replace("'", "''") + "',order1=" + Val(S41.Text.ToString).ToString + ",order2=" + Val(S42.Text.ToString).ToString + ",order3=" + Val(S43.Text.ToString).ToString + ",order4=" + Val(S44.Text.ToString).ToString + ",order5=" + Val(S45.Text.ToString).ToString + ",order6=" + Val(S46.Text.ToString).ToString + ",order7=" + Val(S47.Text.ToString).ToString + ",order8=" + Val(S48.Text.ToString).ToString + ",order9=" + Val(S49.Text.ToString).ToString + ",order10=" + Val(S510.Text.ToString).ToString + " where id=4"
        sQuery = sQuery + " Update clues set clue='" + tbClue5.Text.ToString.Replace("'", "''") + "',Answer='" + tbAnswer5.Text.ToString.Replace("'", "''") + "',order1=" + Val(S51.Text.ToString).ToString + ",order2=" + Val(S52.Text.ToString).ToString + ",order3=" + Val(S53.Text.ToString).ToString + ",order4=" + Val(S54.Text.ToString).ToString + ",order5=" + Val(S55.Text.ToString).ToString + ",order6=" + Val(S56.Text.ToString).ToString + ",order7=" + Val(S57.Text.ToString).ToString + ",order8=" + Val(S58.Text.ToString).ToString + ",order9=" + Val(s59.Text.ToString).ToString + ",order10=" + Val(S510.Text.ToString).ToString + " where id=5"
        sQuery = sQuery + " Update clues set clue='" + tbClue6.Text.ToString.Replace("'", "''") + "',Answer='" + tbAnswer6.Text.ToString.Replace("'", "''") + "',order1=" + Val(S61.Text.ToString).ToString + ",order2=" + Val(S62.Text.ToString).ToString + ",order3=" + Val(S63.Text.ToString).ToString + ",order4=" + Val(S64.Text.ToString).ToString + ",order5=" + Val(S65.Text.ToString).ToString + ",order6=" + Val(S66.Text.ToString).ToString + ",order7=" + Val(S67.Text.ToString).ToString + ",order8=" + Val(S68.Text.ToString).ToString + ",order9=" + Val(S69.Text.ToString).ToString + ",order10=" + Val(S610.Text.ToString).ToString + " where id=6"
        sQuery = sQuery + " Update clues set clue='" + tbClue7.Text.ToString.Replace("'", "''") + "',Answer='" + tbAnswer7.Text.ToString.Replace("'", "''") + "',order1=" + Val(S71.Text.ToString).ToString + ",order2=" + Val(S72.Text.ToString).ToString + ",order3=" + Val(S73.Text.ToString).ToString + ",order4=" + Val(S74.Text.ToString).ToString + ",order5=" + Val(S75.Text.ToString).ToString + ",order6=" + Val(S76.Text.ToString).ToString + ",order7=" + Val(S77.Text.ToString).ToString + ",order8=" + Val(S78.Text.ToString).ToString + ",order9=" + Val(S79.Text.ToString).ToString + ",order10=" + Val(S710.Text.ToString).ToString + " where id=7"
        sQuery = sQuery + " Update clues set clue='" + tbClue8.Text.ToString.Replace("'", "''") + "',Answer='" + tbAnswer8.Text.ToString.Replace("'", "''") + "',order1=" + Val(S81.Text.ToString).ToString + ",order2=" + Val(S82.Text.ToString).ToString + ",order3=" + Val(S83.Text.ToString).ToString + ",order4=" + Val(S84.Text.ToString).ToString + ",order5=" + Val(S85.Text.ToString).ToString + ",order6=" + Val(S86.Text.ToString).ToString + ",order7=" + Val(S87.Text.ToString).ToString + ",order8=" + Val(S88.Text.ToString).ToString + ",order9=" + Val(S89.Text.ToString).ToString + ",order10=" + Val(S810.Text.ToString).ToString + " where id=8"
        sQuery = sQuery + " Update clues set clue='" + tbClue9.Text.ToString.Replace("'", "''") + "',Answer='" + tbAnswer9.Text.ToString.Replace("'", "''") + "',order1=" + Val(S91.Text.ToString).ToString + ",order2=" + Val(S92.Text.ToString).ToString + ",order3=" + Val(S93.Text.ToString).ToString + ",order4=" + Val(S94.Text.ToString).ToString + ",order5=" + Val(S95.Text.ToString).ToString + ",order6=" + Val(S96.Text.ToString).ToString + ",order7=" + Val(S97.Text.ToString).ToString + ",order8=" + Val(S98.Text.ToString).ToString + ",order9=" + Val(S99.Text.ToString).ToString + ",order10=" + Val(S910.Text.ToString).ToString + " where id=9"
        sQuery = sQuery + " Update clues set clue='" + tbClue10.Text.ToString.Replace("'", "''") + "',Answer='" + tbAnswer10.Text.ToString.Replace("'", "''") + "',order1=" + Val(S101.Text.ToString).ToString + ",order2=" + Val(S102.Text.ToString).ToString + ",order3=" + Val(S103.Text.ToString).ToString + ",order4=" + Val(S104.Text.ToString).ToString + ",order5=" + Val(S105.Text.ToString).ToString + ",order6=" + Val(S106.Text.ToString).ToString + ",order7=" + Val(S107.Text.ToString).ToString + ",order8=" + Val(S108.Text.ToString).ToString + ",order9=" + Val(S109.Text.ToString).ToString + ",order10=" + Val(S1010.Text.ToString).ToString + " where id=10"
        GeneralDataQuery(sQuery)
        Me.GetClues()
    End Sub
    Protected Sub Menu1_MenuItemClick(sender As Object, e As MenuEventArgs) Handles Menu1.MenuItemClick
        If e.Item.Value = "Teams" Then
            Response.Redirect("Teams.aspx")
        ElseIf e.Item.Value = "Review" Then
            Response.Redirect("Results.aspx")
        ElseIf e.Item.Value = "StartHunt" Then
            Response.Redirect("Default.aspx")
        ElseIf e.Item.Value = "Clues" Then
            Response.Redirect("Clues.aspx")
        End If
    End Sub

    Protected Sub bSave_Click(sender As Object, e As EventArgs) Handles bSave.Click
        SaveClues()
    End Sub
End Class