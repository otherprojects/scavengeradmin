﻿Public Class Teams
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then GetTeams()
    End Sub
    Private Sub GetTeams()
        Dim ds As DataSet = GeneralDataSet("Select * from teams order by id")
        With ds.Tables(0)
            With .Rows(0)
                tbTeam1.Text = .Item("Name").ToString
                tbColor1.Text = .Item("Color").ToString
                If .Item("Color").ToString <> "" Then ColorBox1.BgColor = "#" + .Item("Color").ToString
            End With
            With .Rows(1)
                tbTeam2.Text = .Item("Name").ToString
                tbColor2.Text = .Item("Color").ToString
                If .Item("Color").ToString <> "" Then ColorBox2.BgColor = "#" + .Item("Color").ToString
            End With
            With .Rows(2)
                tbTeam3.Text = .Item("Name").ToString
                tbColor3.Text = .Item("Color").ToString
                If .Item("Color").ToString <> "" Then ColorBox3.BgColor = "#" + .Item("Color").ToString
            End With
            With .Rows(3)
                tbTeam4.Text = .Item("Name").ToString
                tbColor4.Text = .Item("Color").ToString
                If .Item("Color").ToString <> "" Then ColorBox4.BgColor = "#" + .Item("Color").ToString
            End With
            With .Rows(4)
                tbTeam5.Text = .Item("Name").ToString
                tbColor5.Text = .Item("Color").ToString
                If .Item("Color").ToString <> "" Then ColorBox5.BgColor = "#" + .Item("Color").ToString
            End With
            With .Rows(5)
                tbTeam6.Text = .Item("Name").ToString
                tbColor6.Text = .Item("Color").ToString
                If .Item("Color").ToString <> "" Then ColorBox6.BgColor = "#" + .Item("Color").ToString
            End With
            With .Rows(6)
                tbTeam7.Text = .Item("Name").ToString
                tbColor7.Text = .Item("Color").ToString
                If .Item("Color").ToString <> "" Then ColorBox7.BgColor = "#" + .Item("Color").ToString
            End With
            With .Rows(7)
                tbTeam8.Text = .Item("Name").ToString
                tbColor8.Text = .Item("Color").ToString
                If .Item("Color").ToString <> "" Then ColorBox8.BgColor = "#" + .Item("Color").ToString
            End With
            With .Rows(8)
                tbTeam9.Text = .Item("Name").ToString
                tbColor9.Text = .Item("Color").ToString
                If .Item("Color").ToString <> "" Then ColorBox9.BgColor = "#" + .Item("Color").ToString
            End With
            With .Rows(9)
                tbTeam10.Text = .Item("Name").ToString
                tbColor10.Text = .Item("Color").ToString
                If .Item("Color").ToString <> "" Then ColorBox10.BgColor = "#" + .Item("Color").ToString
            End With
        End With
    End Sub
    Protected Sub Menu1_MenuItemClick(sender As Object, e As MenuEventArgs) Handles Menu1.MenuItemClick
        If e.Item.Value = "Teams" Then
            Response.Redirect("Teams.aspx")
        ElseIf e.Item.Value = "Review" Then
            Response.Redirect("Results.aspx")
        ElseIf e.Item.Value = "StartHunt" Then
            Response.Redirect("Default.aspx")
        ElseIf e.Item.Value = "Clues" Then
            Response.Redirect("Clues.aspx")

        End If

    End Sub

    Protected Sub bSave_Click(sender As Object, e As EventArgs) Handles bSave.Click
        Dim sQuery As String = "update teams set name='" + tbTeam1.Text.ToString + "',color='" + tbColor1.Text.ToString + "' where id=1"
        sQuery = sQuery + " update teams set name='" + tbTeam2.Text.ToString + "',color='" + tbColor2.Text.ToString + "' where id=2"
        sQuery = sQuery + " update teams set name='" + tbTeam3.Text.ToString + "',color='" + tbColor3.Text.ToString + "' where id=3"
        sQuery = sQuery + " update teams set name='" + tbTeam4.Text.ToString + "',color='" + tbColor4.Text.ToString + "' where id=4"
        sQuery = sQuery + " update teams set name='" + tbTeam5.Text.ToString + "',color='" + tbColor5.Text.ToString + "' where id=5"
        sQuery = sQuery + " update teams set name='" + tbTeam6.Text.ToString + "',color='" + tbColor6.Text.ToString + "' where id=6"
        sQuery = sQuery + " update teams set name='" + tbTeam7.Text.ToString + "',color='" + tbColor7.Text.ToString + "' where id=7"
        sQuery = sQuery + " update teams set name='" + tbTeam8.Text.ToString + "',color='" + tbColor8.Text.ToString + "' where id=8"
        sQuery = sQuery + " update teams set name='" + tbTeam9.Text.ToString + "',color='" + tbColor9.Text.ToString + "' where id=9"
        sQuery = sQuery + " update teams set name='" + tbTeam10.Text.ToString + "',color='" + tbColor10.Text.ToString + "' where id=10"

        GeneralDataQuery(sQuery)

        GetTeams()
    End Sub
End Class