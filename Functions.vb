﻿Module Functions
    Public cn As System.Data.SqlClient.SqlConnection
    Public Function DataConnection() As System.Data.SqlClient.SqlConnection
        DataConnection = New System.Data.SqlClient.SqlConnection
        DataConnection.ConnectionString = My.Settings.ConnectionString
        DataConnection.Open()
    End Function
    Public Function GeneralDataSet(ByVal QueryString As String, Optional ByVal TableName As String = "") As DataSet
        Try
            GeneralDataSet = New DataSet
            Dim Connection As SqlClient.SqlConnection
            Connection = DataConnection()
            Dim SelectCommand As New SqlClient.SqlCommand(QueryString, Connection)
            Dim da As New SqlClient.SqlDataAdapter
            da.SelectCommand = SelectCommand
            If TableName = "" Then
                da.Fill(GeneralDataSet)
            Else
                da.Fill(GeneralDataSet, TableName)
            End If
            Connection.Close()
            Connection.Dispose()
        Catch ex As Exception
            LogError(ex, "GeneralDataQuery:" + QueryString)
            Return New DataSet
        End Try
    End Function
    Public Function GeneralDataQuery(ByVal QueryString As String, Optional ByVal TableName As String = "") As String
        Try
            Dim Connection As SqlClient.SqlConnection
            Connection = DataConnection()
            Dim SelectCommand As New SqlClient.SqlCommand(QueryString, Connection)
            GeneralDataQuery = SelectCommand.ExecuteNonQuery().ToString
            Connection.Close()
            Connection.Dispose()
        Catch ex As Exception
            LogError(ex, "GeneralDataQuery:" + QueryString)
            Return "0"
        End Try
    End Function
    Public Function LogError(ex As Exception, Optional CalledFrom As String = "", Optional Other As String = "")
        Try
            Dim sSource As String = "RRSCavenger REST Service"
            Dim sLog As String = "Application"
            Dim sEvent As String = ex.Message + vbCrLf + ex.StackTrace + IIf(CalledFrom = "", "", vbCrLf + "Called From: " + CalledFrom) + IIf(Other = "", "", vbCrLf + Other)
            Dim sMachine As String = "."
            If Not EventLog.SourceExists(sSource, sMachine) Then
                Dim cs As New EventSourceCreationData(sSource, sLog)
                System.Diagnostics.EventLog.CreateEventSource(cs)
            End If
            Dim ELog As New EventLog(sLog, sMachine, sSource)
            ELog.WriteEntry(sEvent, EventLogEntryType.Warning, 234, CType(3, Short))
        Catch
        End Try
        Try
            Dim appPath As String = HttpContext.Current.Request.ApplicationPath
            Dim mapPath As String = HttpContext.Current.Request.MapPath(appPath)
            mapPath = mapPath + IIf(mapPath.EndsWith("\"), "", "\") + "Error.txt"
            My.Computer.FileSystem.WriteAllText(mapPath, Now.ToShortDateString + " " + Now.ToShortTimeString + " - " + ex.Message + vbCrLf + ex.StackTrace + IIf(CalledFrom = "", "", vbCrLf + "Called From: " + CalledFrom) + IIf(Other = "", "", vbCrLf + Other) + vbCrLf, True)
        Catch
        End Try
    End Function
End Module
