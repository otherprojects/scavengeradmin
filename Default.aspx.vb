﻿Public Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then GetData()
    End Sub
    Private Sub GetData()
        Dim ds As DataSet = GeneralDataSet("select top 1 ready,secret from ready")
        cbHunt.Checked = ds.Tables(0).Rows(0).Item("ready")
        tbSecret.Text = ds.Tables(0).Rows(0).Item("secret")
    End Sub

    Protected Sub bSave_Click(sender As Object, e As EventArgs) Handles bSave.Click
        GeneralDataSet("update ready set ready=" + IIf(cbHunt.Checked, "1", "0") + ",secret='" + tbSecret.Text + "'")
    End Sub
    Protected Sub Menu1_MenuItemClick(sender As Object, e As MenuEventArgs) Handles Menu1.MenuItemClick
        If e.Item.Value = "Teams" Then
            Response.Redirect("Teams.aspx")
        ElseIf e.Item.Value = "Review" Then
            Response.Redirect("Results.aspx")
        ElseIf e.Item.Value = "StartHunt" Then
            Response.Redirect("Default.aspx")
        ElseIf e.Item.Value = "Clues" Then
            Response.Redirect("Clues.aspx")

        End If
    End Sub
End Class