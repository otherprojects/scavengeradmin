﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Teams.aspx.vb" Inherits="ScavengerAdmin.Teams" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <script type="text/javascript">
        function ColorChange(id) {
            if (document.getElementById('tbColor' + id).value.length == 6) {
                document.getElementById('ColorBox' + id).style.backgroundColor = '#' + document.getElementById('tbColor' + id).value;
            }
        }

    </script>
    <form id="form1" runat="server">
   <div>
        <div style="background-color:#FFFBD6">
        <asp:Menu ID="Menu1" runat="server" BackColor="#FFFBD6" DynamicHorizontalOffset="2" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#990000" StaticSubMenuIndent="10px">
            <DynamicHoverStyle BackColor="#990000" ForeColor="White" />
            <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
            <DynamicMenuStyle BackColor="#FFFBD6" />
            <DynamicSelectedStyle BackColor="#FFCC66" />
            <Items>
                <asp:MenuItem Text="Start the Hunt" Value="StartHunt" Selected="true"></asp:MenuItem>
                <asp:MenuItem Text="Teams" Value="Teams" Selected="true"></asp:MenuItem>
                <asp:MenuItem Text="Clues" Value="Clues"></asp:MenuItem>
                <asp:MenuItem Text="Review Results" Value="Review"></asp:MenuItem>
            </Items>
            <StaticHoverStyle BackColor="#990000" ForeColor="White" />
            <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
            <StaticSelectedStyle BackColor="#FFCC66" />
        </asp:Menu>
            <br />
       </div>
    <table>
        <tr><td rowspan="20"><img alt="" class="auto-style1" src="HiRes-Monkey.gif" /></td><td style="font-size:x-large" colspan="2">Scavenger Hunt Teams</td></tr>
        <tr ><td>Team 1: </td><td><asp:TextBox ID="tbTeam1" runat="server"></asp:TextBox></td><td>Color Code:</td><td><asp:TextBox ID="tbColor1" runat="server" onkeyup="ColorChange('1');"></asp:TextBox></td><td id="ColorBox1" runat="server" style="width:30px"></td></tr>
        <tr ><td>Team 2: </td><td><asp:TextBox ID="tbTeam2" runat="server"></asp:TextBox></td><td>Color Code:</td><td><asp:TextBox ID="tbColor2" runat="server" onkeyup="ColorChange('2');"></asp:TextBox></td><td id="ColorBox2" runat="server" style="width:30px"></td></tr>
        <tr ><td>Team 3: </td><td><asp:TextBox ID="tbTeam3" runat="server"></asp:TextBox></td><td>Color Code:</td><td><asp:TextBox ID="tbColor3" runat="server" onkeyup="ColorChange('3');"></asp:TextBox></td><td id="ColorBox3" runat="server" style="width:30px"></td></tr>
        <tr ><td>Team 4: </td><td><asp:TextBox ID="tbTeam4" runat="server"></asp:TextBox></td><td>Color Code:</td><td><asp:TextBox ID="tbColor4" runat="server" onkeyup="ColorChange('4');"></asp:TextBox></td><td id="ColorBox4" runat="server" style="width:30px"></td></tr>
        <tr ><td>Team 5: </td><td><asp:TextBox ID="tbTeam5" runat="server"></asp:TextBox></td><td>Color Code:</td><td><asp:TextBox ID="tbColor5" runat="server" onkeyup="ColorChange('5');"></asp:TextBox></td><td id="ColorBox5" runat="server" style="width:30px"></td></tr>
        <tr ><td>Team 6: </td><td><asp:TextBox ID="tbTeam6" runat="server"></asp:TextBox></td><td>Color Code:</td><td><asp:TextBox ID="tbColor6" runat="server" onkeyup="ColorChange('6');"></asp:TextBox></td><td id="ColorBox6" runat="server" style="width:30px"></td></tr>
        <tr ><td>Team 7: </td><td><asp:TextBox ID="tbTeam7" runat="server"></asp:TextBox></td><td>Color Code:</td><td><asp:TextBox ID="tbColor7" runat="server" onkeyup="ColorChange('7');"></asp:TextBox></td><td id="ColorBox7" runat="server" style="width:30px"></td></tr>
        <tr ><td>Team 8: </td><td><asp:TextBox ID="tbTeam8" runat="server"></asp:TextBox></td><td>Color Code:</td><td><asp:TextBox ID="tbColor8" runat="server" onkeyup="ColorChange('8');"></asp:TextBox></td><td id="ColorBox8" runat="server" style="width:30px"></td></tr>
        <tr ><td>Team 9: </td><td><asp:TextBox ID="tbTeam9" runat="server"></asp:TextBox></td><td>Color Code:</td><td><asp:TextBox ID="tbColor9" runat="server" onkeyup="ColorChange('9');"></asp:TextBox></td><td id="ColorBox9" runat="server" style="width:30px"></td></tr>
        <tr ><td>Team 10: </td><td><asp:TextBox ID="tbTeam10" runat="server"></asp:TextBox></td><td>Color Code:</td><td><asp:TextBox ID="tbColor10" runat="server" onkeyup="ColorChange('10');"></asp:TextBox></td><td id="ColorBox10" runat="server" style="width:30px"></td></tr>
        <tr><td></td><td><asp:Button ID="bSave" runat="server" Text="Save" /></td><td><input id="Button1" type="button" value="Color Codes" onclick="window.open('http://www.quackit.com/css/css_color_codes.cfm');" /></td></tr>
    </table>
    </div> </form>
</body>
</html>
