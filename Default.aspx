﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="ScavengerAdmin._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 255px;
            height: 330px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div style="background-color:#FFFBD6">
            <br />
        <asp:Menu ID="Menu1" runat="server" BackColor="#FFFBD6" DynamicHorizontalOffset="2" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#990000" StaticSubMenuIndent="10px">
            <DynamicHoverStyle BackColor="#990000" ForeColor="White" />
            <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
            <DynamicMenuStyle BackColor="#FFFBD6" />
            <DynamicSelectedStyle BackColor="#FFCC66" />
            <Items>
                <asp:MenuItem Text="Start the Hunt" Value="StartHunt" Selected="true"></asp:MenuItem>
                <asp:MenuItem Text="Teams" Value="Teams"></asp:MenuItem>
                <asp:MenuItem Text="Clues" Value="Clues"></asp:MenuItem>
                <asp:MenuItem Text="Review Results" Value="Review"></asp:MenuItem>
            </Items>
            <StaticHoverStyle BackColor="#990000" ForeColor="White" />
            <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
            <StaticSelectedStyle BackColor="#FFCC66" />
        </asp:Menu>
            <br />
       </div>
    <table>
        <tr><td rowspan="4"><img alt="" class="auto-style1" src="HiRes-Monkey.gif" /></td><td style="font-size:x-large" colspan="2">Scavenger Hunt Administration</td></tr>
        <tr ><td>
            Secret Word: </td>
            <td>
                <asp:TextBox ID="tbSecret" runat="server"></asp:TextBox></td>
        </tr>
        <tr><td colspan="2">
            <asp:CheckBox ID="cbHunt" TextAlign="Left" Text="Turn on hunt" runat="server" /></td></tr>
        <tr><td></td><td><asp:Button ID="bSave" runat="server" Text="Save" /></td></tr>
    </table>
        
    </div>
    </form>
</body>
</html>
