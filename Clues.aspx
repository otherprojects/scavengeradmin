﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Clues.aspx.vb" Inherits="ScavengerAdmin.Clues" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
                  <div style="background-color:#FFFBD6">
        <asp:Menu ID="Menu1" runat="server" BackColor="#FFFBD6" DynamicHorizontalOffset="2" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#990000" StaticSubMenuIndent="10px">
            <DynamicHoverStyle BackColor="#990000" ForeColor="White" />
            <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
            <DynamicMenuStyle BackColor="#FFFBD6" />
            <DynamicSelectedStyle BackColor="#FFCC66" />
            <Items>
                <asp:MenuItem Text="Start the Hunt" Value="StartHunt" Selected="true"></asp:MenuItem>
                <asp:MenuItem Text="Teams" Value="Teams"></asp:MenuItem>
                <asp:MenuItem Text="Clues" Value="Clues" Selected="true"></asp:MenuItem>
                <asp:MenuItem Text="Review Results" Value="Review"></asp:MenuItem>
            </Items>
            <StaticHoverStyle BackColor="#990000" ForeColor="White" />
            <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
            <StaticSelectedStyle BackColor="#FFCC66" />
        </asp:Menu>
                <br />
       </div>
        <div style="font-size:large">Scavenger Hunt Clues</div>
        <asp:Button ID="bSave" runat="server" Text="Save" />
    <table>
        <tr><td style="font-weight:bold;background-color:blue;color:white;" colspan="11">Location 1</td></tr>
        <tr><td>Clue:</td><td colspan="11"><asp:TextBox ID="tbClue1" Width="800px"  runat="server"></asp:TextBox></td></tr>
        <tr><td>Answer:</td><td colspan="11"><asp:TextBox ID="tbAnswer1" Width="800px"  runat="server"></asp:TextBox></td></tr>
        <tr><td>Sequence</td><td>1<asp:TextBox ID="S11" runat="server" Width="30px"></asp:TextBox></td><td>2<asp:TextBox ID="S12" runat="server" Width="30px"></asp:TextBox></td><td>3<asp:TextBox ID="S13" runat="server" Width="30px"></asp:TextBox></td><td>4<asp:TextBox ID="S14" runat="server" Width="30px"></asp:TextBox></td><td>5<asp:TextBox ID="S15" runat="server" Width="30px"></asp:TextBox></td><td>6<asp:TextBox ID="S16" runat="server" Width="30px"></asp:TextBox></td><td>7<asp:TextBox ID="S17" runat="server" Width="30px"></asp:TextBox></td><td>8<asp:TextBox ID="S18" runat="server" Width="30px"></asp:TextBox></td><td>9<asp:TextBox ID="S19" runat="server" Width="30px"></asp:TextBox></td><td>10<asp:TextBox ID="S110" runat="server" Width="30px"></asp:TextBox></td></tr>

        <tr><td style="font-weight:bold;background-color:blue;color:white" colspan="11">Location 2</td></tr>
        <tr><td>Clue:</td><td colspan="11"><asp:TextBox ID="tbClue2" Width="800px"  runat="server"></asp:TextBox></td></tr>
        <tr><td>Answer:</td><td colspan="11"><asp:TextBox ID="tbAnswer2" Width="800px"  runat="server"></asp:TextBox></td></tr>
        <tr><td>Sequence</td><td>1<asp:TextBox ID="S21" runat="server" Width="30px"></asp:TextBox></td><td>2<asp:TextBox ID="S22" runat="server" Width="30px"></asp:TextBox></td><td>3<asp:TextBox ID="S23" runat="server" Width="30px"></asp:TextBox></td><td>4<asp:TextBox ID="S24" runat="server" Width="30px"></asp:TextBox></td><td>5<asp:TextBox ID="S25" runat="server" Width="30px"></asp:TextBox></td><td>6<asp:TextBox ID="S26" runat="server" Width="30px"></asp:TextBox></td><td>7<asp:TextBox ID="S27" runat="server" Width="30px"></asp:TextBox></td><td>8<asp:TextBox ID="S28" runat="server" Width="30px"></asp:TextBox></td><td>9<asp:TextBox ID="S29" runat="server" Width="30px"></asp:TextBox></td><td>10<asp:TextBox ID="S210" runat="server" Width="30px"></asp:TextBox></td></tr>

        <tr><td style="font-weight:bold;background-color:blue;color:white" colspan="11">Location 3</td></tr>
        <tr><td>Clue:</td><td colspan="11"><asp:TextBox ID="tbClue3" Width="800px"  runat="server"></asp:TextBox></td></tr>
        <tr><td>Answer:</td><td colspan="11"><asp:TextBox ID="tbAnswer3" Width="800px"  runat="server"></asp:TextBox></td></tr>
        <tr><td>Sequence</td><td>1<asp:TextBox ID="S31" runat="server" Width="30px"></asp:TextBox></td><td>2<asp:TextBox ID="S32" runat="server" Width="30px"></asp:TextBox></td><td>3<asp:TextBox ID="S33" runat="server" Width="30px"></asp:TextBox></td><td>4<asp:TextBox ID="S34" runat="server" Width="30px"></asp:TextBox></td><td>5<asp:TextBox ID="S35" runat="server" Width="30px"></asp:TextBox></td><td>6<asp:TextBox ID="S36" runat="server" Width="30px"></asp:TextBox></td><td>7<asp:TextBox ID="S37" runat="server" Width="30px"></asp:TextBox></td><td>8<asp:TextBox ID="S38" runat="server" Width="30px"></asp:TextBox></td><td>9<asp:TextBox ID="S39" runat="server" Width="30px"></asp:TextBox></td><td>10<asp:TextBox ID="S310" runat="server" Width="30px"></asp:TextBox></td></tr>

        <tr><td style="font-weight:bold;background-color:blue;color:white" colspan="11">Location 4</td></tr>
        <tr><td>Clue:</td><td colspan="11"><asp:TextBox ID="tbClue4" Width="800px"  runat="server"></asp:TextBox></td></tr>
        <tr><td>Answer:</td><td colspan="11"><asp:TextBox ID="tbAnswer4" Width="800px"  runat="server"></asp:TextBox></td></tr>
        <tr><td>Sequence</td><td>1<asp:TextBox ID="S41" runat="server" Width="30px"></asp:TextBox></td><td>2<asp:TextBox ID="S42" runat="server" Width="30px"></asp:TextBox></td><td>3<asp:TextBox ID="S43" runat="server" Width="30px"></asp:TextBox></td><td>4<asp:TextBox ID="S44" runat="server" Width="30px"></asp:TextBox></td><td>5<asp:TextBox ID="S45" runat="server" Width="30px"></asp:TextBox></td><td>6<asp:TextBox ID="S46" runat="server" Width="30px"></asp:TextBox></td><td>7<asp:TextBox ID="S47" runat="server" Width="30px"></asp:TextBox></td><td>8<asp:TextBox ID="S48" runat="server" Width="30px"></asp:TextBox></td><td>9<asp:TextBox ID="S49" runat="server" Width="30px"></asp:TextBox></td><td>10<asp:TextBox ID="S410" runat="server" Width="30px"></asp:TextBox></td></tr>

        <tr><td style="font-weight:bold;background-color:blue;color:white" colspan="11">Location 5</td></tr>
        <tr><td>Clue:</td><td colspan="11"><asp:TextBox ID="tbClue5" Width="800px"  runat="server"></asp:TextBox></td></tr>
        <tr><td>Answer:</td><td colspan="11"><asp:TextBox ID="tbAnswer5" Width="800px"  runat="server"></asp:TextBox></td></tr>
        <tr><td>Sequence</td><td>1<asp:TextBox ID="S51" runat="server" Width="30px"></asp:TextBox></td><td>2<asp:TextBox ID="S52" runat="server" Width="30px"></asp:TextBox></td><td>3<asp:TextBox ID="S53" runat="server" Width="30px"></asp:TextBox></td><td>4<asp:TextBox ID="S54" runat="server" Width="30px"></asp:TextBox></td><td>5<asp:TextBox ID="S55" runat="server" Width="30px"></asp:TextBox></td><td>6<asp:TextBox ID="S56" runat="server" Width="30px"></asp:TextBox></td><td>7<asp:TextBox ID="S57" runat="server" Width="30px"></asp:TextBox></td><td>8<asp:TextBox ID="S58" runat="server" Width="30px"></asp:TextBox></td><td>9<asp:TextBox ID="s59" runat="server" Width="30px"></asp:TextBox></td><td>10<asp:TextBox ID="S510" runat="server" Width="30px"></asp:TextBox></td></tr>

        <tr><td style="font-weight:bold;background-color:blue;color:white" colspan="11">Location 6</td></tr>
        <tr><td>Clue:</td><td colspan="11"><asp:TextBox ID="tbClue6" Width="800px"  runat="server"></asp:TextBox></td></tr>
        <tr><td>Answer:</td><td colspan="11"><asp:TextBox ID="tbAnswer6" Width="800px"  runat="server"></asp:TextBox></td></tr>
        <tr><td>Sequence</td><td>1<asp:TextBox ID="S61" runat="server" Width="30px"></asp:TextBox></td><td>2<asp:TextBox ID="S62" runat="server" Width="30px"></asp:TextBox></td><td>3<asp:TextBox ID="S63" runat="server" Width="30px"></asp:TextBox></td><td>4<asp:TextBox ID="S64" runat="server" Width="30px"></asp:TextBox></td><td>5<asp:TextBox ID="S65" runat="server" Width="30px"></asp:TextBox></td><td>6<asp:TextBox ID="S66" runat="server" Width="30px"></asp:TextBox></td><td>7<asp:TextBox ID="S67" runat="server" Width="30px"></asp:TextBox></td><td>8<asp:TextBox ID="S68" runat="server" Width="30px"></asp:TextBox></td><td>9<asp:TextBox ID="S69" runat="server" Width="30px"></asp:TextBox></td><td>10<asp:TextBox ID="S610" runat="server" Width="30px"></asp:TextBox></td></tr>

        <tr><td style="font-weight:bold;background-color:blue;color:white" colspan="11">Location 7</td></tr>
        <tr><td>Clue:</td><td colspan="11"><asp:TextBox ID="tbClue7" Width="800px"  runat="server"></asp:TextBox></td></tr>
        <tr><td>Answer:</td><td colspan="11"><asp:TextBox ID="tbAnswer7" Width="800px"  runat="server"></asp:TextBox></td></tr>
        <tr><td>Sequence</td><td>1<asp:TextBox ID="S71" runat="server" Width="30px"></asp:TextBox></td><td>2<asp:TextBox ID="S72" runat="server" Width="30px"></asp:TextBox></td><td>3<asp:TextBox ID="S73" runat="server" Width="30px"></asp:TextBox></td><td>4<asp:TextBox ID="S74" runat="server" Width="30px"></asp:TextBox></td><td>5<asp:TextBox ID="S75" runat="server" Width="30px"></asp:TextBox></td><td>6<asp:TextBox ID="S76" runat="server" Width="30px"></asp:TextBox></td><td>7<asp:TextBox ID="S77" runat="server" Width="30px"></asp:TextBox></td><td>8<asp:TextBox ID="S78" runat="server" Width="30px"></asp:TextBox></td><td>9<asp:TextBox ID="S79" runat="server" Width="30px"></asp:TextBox></td><td>10<asp:TextBox ID="S710" runat="server" Width="30px"></asp:TextBox></td></tr>

        <tr><td style="font-weight:bold;background-color:blue;color:white" colspan="11">Location 8</td></tr>
        <tr><td>Clue:</td><td colspan="11"><asp:TextBox ID="tbClue8" Width="800px"  runat="server"></asp:TextBox></td></tr>
        <tr><td>Answer:</td><td colspan="11"><asp:TextBox ID="tbAnswer8" Width="800px"  runat="server"></asp:TextBox></td></tr>
        <tr><td>Sequence</td><td>1<asp:TextBox ID="S81" runat="server" Width="30px"></asp:TextBox></td><td>2<asp:TextBox ID="S82" runat="server" Width="30px"></asp:TextBox></td><td>3<asp:TextBox ID="S83" runat="server" Width="30px"></asp:TextBox></td><td>4<asp:TextBox ID="S84" runat="server" Width="30px"></asp:TextBox></td><td>5<asp:TextBox ID="S85" runat="server" Width="30px"></asp:TextBox></td><td>6<asp:TextBox ID="S86" runat="server" Width="30px"></asp:TextBox></td><td>7<asp:TextBox ID="S87" runat="server" Width="30px"></asp:TextBox></td><td>8<asp:TextBox ID="S88" runat="server" Width="30px"></asp:TextBox></td><td>9<asp:TextBox ID="S89" runat="server" Width="30px"></asp:TextBox></td><td>10<asp:TextBox ID="S810" runat="server" Width="30px"></asp:TextBox></td></tr>

        <tr><td style="font-weight:bold;background-color:blue;color:white" colspan="11">Location 9</td></tr>
        <tr><td>Clue:</td><td colspan="11"><asp:TextBox ID="tbClue9" Width="800px"  runat="server"></asp:TextBox></td></tr>
        <tr><td>Answer:</td><td colspan="11"><asp:TextBox ID="tbAnswer9" Width="800px"  runat="server"></asp:TextBox></td></tr>
        <tr><td>Sequence</td><td>1<asp:TextBox ID="S91" runat="server" Width="30px"></asp:TextBox></td><td>2<asp:TextBox ID="S92" runat="server" Width="30px"></asp:TextBox></td><td>3<asp:TextBox ID="S93" runat="server" Width="30px"></asp:TextBox></td><td>4<asp:TextBox ID="S94" runat="server" Width="30px"></asp:TextBox></td><td>5<asp:TextBox ID="S95" runat="server" Width="30px"></asp:TextBox></td><td>6<asp:TextBox ID="S96" runat="server" Width="30px"></asp:TextBox></td><td>7<asp:TextBox ID="S97" runat="server" Width="30px"></asp:TextBox></td><td>8<asp:TextBox ID="S98" runat="server" Width="30px"></asp:TextBox></td><td>9<asp:TextBox ID="S99" runat="server" Width="30px"></asp:TextBox></td><td>10<asp:TextBox ID="S910" runat="server" Width="30px"></asp:TextBox></td></tr>

        <tr><td style="font-weight:bold;background-color:blue;color:white" colspan="11">Location 10</td></tr>
        <tr><td>Clue:</td><td colspan="11"><asp:TextBox ID="tbClue10" Width="800px"  runat="server"></asp:TextBox></td></tr>
        <tr><td>Answer:</td><td colspan="11"><asp:TextBox ID="tbAnswer10" Width="800px"  runat="server"></asp:TextBox></td></tr>
        <tr><td>Sequence</td><td>1<asp:TextBox ID="S101" runat="server" Width="30px"></asp:TextBox></td><td>2<asp:TextBox ID="S102" runat="server" Width="30px"></asp:TextBox></td><td>3<asp:TextBox ID="S103" runat="server" Width="30px"></asp:TextBox></td><td>4<asp:TextBox ID="S104" runat="server" Width="30px"></asp:TextBox></td><td>5<asp:TextBox ID="S105" runat="server" Width="30px"></asp:TextBox></td><td>6<asp:TextBox ID="S106" runat="server" Width="30px"></asp:TextBox></td><td>7<asp:TextBox ID="S107" runat="server" Width="30px"></asp:TextBox></td><td>8<asp:TextBox ID="S108" runat="server" Width="30px"></asp:TextBox></td><td>9<asp:TextBox ID="S109" runat="server" Width="30px"></asp:TextBox></td><td>10<asp:TextBox ID="S1010" runat="server" Width="30px"></asp:TextBox></td></tr>
    </table>
    </div>
    </form>
</body>
</html>
